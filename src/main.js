import Vue from 'vue'
import Application from './Application'
import router from './router'
import store from './store'
import VueCookie from 'vue-cookie'

Vue.use(VueCookie)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(Application)
})
