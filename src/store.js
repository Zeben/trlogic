import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    SYMBOLS: {
      'USD': '$',
      'EUR': '€',
      'RUR': '₽',
      'BTC': 'Ƀ'
    },
    currencies: [],
    availableMoney: '',
    history: []
  },

  mutations: {
    pushData(state, { field, payload }) {
      state[field] = payload
    },

    addToHistory(state, payload) {
      state.history.push(payload)
    }
  },

  actions: {
    fetchCurrencies() {
      fetch('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11')
        .then(response => response.json())
        .then(response => {
          this.commit('pushData', { field: 'currencies', payload: response })
        })
    }
  }
})

export default store
