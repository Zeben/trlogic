import Vue from 'vue'
import VueRouter from 'vue-router'

import Root from './components/Root'
import Currency from './components/Currency'
import Input from './components/Input'
import Result from './components/Result'

Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Root },
    { path: '/currency', component: Currency },
    { path: '/input', component: Input },
    { path: '/result', component: Result }
  ]
})

export default router
